#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

int main() {
	//ofstream outputFile ("OutputData.csv");
	//outputFile << "Time, Velocity, X, Y, \n";
	vector<vector<Point> > contours;
	int timestamp = 0;
	const int goalOffset = 28;
	float currDistanceFromGoal = 0;
	float shortestDistanceFromGoal = INT32_MAX;
	string state = "None yet plz";
	bool currState = false;
	bool prevState = false;
	bool twoStates = false;
	bool threeStates = false;
	bool fourStates = false;
	bool fiveStates = false;
	bool sixStates = false;
	int shortX = INT32_MIN;
	int shortY = INT32_MIN;
	float velocity = 0;
	static const float pixelsPerMeter = 114 / .96f;
	static const float framesPerSecond = 30;
	float angle = 0;
	vector<Vec4i> hierarchy;
	cv::VideoCapture cap(0); // camera 0
	int frame_width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
	int frame_height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
	//VideoWriter video("BallVision1.avi", CV_FOURCC('M', 'J', 'P', 'G'), 30, Size(frame_width, frame_height), true);
	//VideoWriter video1("Original.avi", CV_FOURCC('M', 'J', 'P', 'G'), 30, Size(frame_width, frame_height), true);
	cv::Mat img, hsvimg, newhsv, thresholded_img, imgwithoutgreen, imgwithoutgreenhsv, bluehsv;
	vector<int> x_coords, y_coords;
	int iLastX = -1;
	int iLastY = -1;
	Mat imgTmp;
	cap.read(imgTmp);
	int time = 0;
	while (true) {
		Mat imgLines = Mat::zeros(imgTmp.size(), CV_8UC3);
		cap >> img;
		GaussianBlur(img, img, Size(13, 13), 1, 1);
		cv::flip(img, img, 1);
		cv::cvtColor(img, hsvimg, COLOR_RGB2HSV_FULL);
		//inRange(hsvimg, cv::Scalar(29, 86, 6), cv::Scalar(64, 255, 255), newhsv);
		//inRange(hsvimg, cv::Scalar(159, 100, 100), cv::Scalar(179, 255, 255), newhsv);
		inRange(hsvimg, cv::Scalar(129, 100, 100), cv::Scalar(199, 255, 255), newhsv);
		inRange(img, cv::Scalar(200, 80, 0), cv::Scalar(255, 200, 190), bluehsv);
		erode(newhsv, newhsv, Mat(), Point(-1,-1), 2);
		dilate(newhsv, newhsv, Mat(), Point(-1, -1), 2);
		erode(bluehsv, bluehsv, Mat(), Point(-1, -1), 2);
		dilate(bluehsv, bluehsv, Mat(), Point(-1, -1), 2);
		//findContours(newhsv, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		Moments oMoments = moments(newhsv);
		Moments bMoments = moments(bluehsv);

		double dM01 = oMoments.m01;
		double dM10 = oMoments.m10;
		double dArea = oMoments.m00;

		double Goal01 = bMoments.m01;
		double Goal10 = bMoments.m10;
		double GoalArea = bMoments.m00;

		int posX = dM10 / dArea;
		int posY = dM01 / dArea;

		int goalX = Goal01 / GoalArea;
		int goalY = Goal10 / GoalArea;

		x_coords.push_back(posX);
		y_coords.push_back(posY);

		currDistanceFromGoal = sqrt(pow((goalX + goalOffset) - posX, 2) + pow((goalY - goalOffset) - posY, 2));
		bool rest = x_coords.size() == 1 || ((abs(posX - iLastX) + abs(posY - iLastY)) < 3);
		sixStates = fiveStates;
		fiveStates = fourStates;
		fourStates = threeStates;
		threeStates = twoStates;
		twoStates = prevState;
		prevState = currState;
		currState = rest;
		if (currDistanceFromGoal < 1000 && currDistanceFromGoal < shortestDistanceFromGoal && !rest && posX != INT32_MIN && posY != INT32_MIN) {
			printf("IN HERE \n \n \n");
			shortestDistanceFromGoal = currDistanceFromGoal;
			shortX = posX;
			shortY = posY;
		}
		
		if (iLastX >= 0 && iLastY >= 0 && posX >= 0 && posY >= 0)
		{
			auto velocityPixelPerFrame = sqrt(pow((posX - iLastX), 2) + pow((posY - iLastY), 2));
			if (abs(posX - iLastX) + abs(posY - iLastY) > 2 && posX != INT32_MIN && posY != INT32_MIN) {
				velocity = (velocityPixelPerFrame / pixelsPerMeter) * framesPerSecond;
			}
			else {
				velocity = 0;
			}
				//Draw a red line from the previous point to the current point
			if (x_coords.size() >= 6) {
				line(imgLines, Point(x_coords[x_coords.size() - 5], y_coords[y_coords.size() - 5]), Point(x_coords[x_coords.size() - 6], y_coords[y_coords.size() - 6]), Scalar(0, 0, 255), 2);
			}
			if (x_coords.size() >= 5) {
				line(imgLines, Point(x_coords[x_coords.size() - 4], y_coords[y_coords.size() - 4]), Point(x_coords[x_coords.size() - 5], y_coords[y_coords.size() - 5]), Scalar(0, 0, 255), 2);
			}
			if (x_coords.size() >= 4) {
				line(imgLines, Point(x_coords[x_coords.size() - 3], y_coords[y_coords.size() - 3]), Point(x_coords[x_coords.size() - 4], y_coords[y_coords.size() - 4]), Scalar(0, 0, 255), 2);
			}
			if (x_coords.size() >= 3) {
				line(imgLines, Point(x_coords[x_coords.size() - 2], y_coords[y_coords.size() - 2]), Point(x_coords[x_coords.size() - 3], y_coords[y_coords.size() - 3]), Scalar(0, 0, 255), 2);
			}
			if (x_coords.size() >= 2) {
				line(imgLines, Point(iLastX, iLastY), Point(x_coords[x_coords.size() - 2], y_coords[y_coords.size() - 2]), Scalar(0, 0, 255), 2);
			}
			line(imgLines, Point(posX, posY), Point(iLastX, iLastY), Scalar(0, 0, 255), 2);
		}

		iLastX = posX;
		iLastY = posY;


		imshow("Thresholded Image", newhsv); //show the thresholded image

		Mat imgOriginal = img + imgLines;
		//video.write(imgOriginal);
		//video1.write(img);
		char text[255];
		if (posX>0 && posY>0)
			//outputFile << timestamp << "," << velocity << "," << posX << "," << posY << "\n";
		timestamp++;
		printf("Velocity: %f, \n Current x Pos: %d, \n Current y Pos: %d \n, %d, %d, %d \n %d, %d \n %s \n", (float)velocity, (int)posX, (int)posY, goalX, goalY, rest, shortX, shortY, state);
		if (!sixStates && fiveStates && fourStates && threeStates && twoStates && prevState && currState && shortX != INT32_MIN && shortY != INT32_MIN) {
			if (goalX + goalOffset - shortX >= 5) {
				state = "Left";
			}
			else if (goalX + goalOffset - shortX <= -5) {
				state = "Right";
			}
			else {
				state = "Close to center";
			}
			shortestDistanceFromGoal = INT32_MAX;
		}
		else if (posX == INT32_MIN && posY == INT32_MIN && shortX != INT32_MIN && shortY != INT32_MIN) {
			if (goalX + goalOffset - shortX >= 5) {
				state = "Left";
			}
			else if (goalX + goalOffset - shortX <= -5) {
				state = "Right";
			}
			else {
				state = "Close to center";
			}
			shortestDistanceFromGoal = INT32_MAX;
		}
		/*CvFont font;
		double hScale = 1.0;
		double vScale = 1.0;
		int    lineWidth = 1;
		putText(imgOriginal, text, cvPoint(200, 400), FONT_HERSHEY_SIMPLEX, hScale, cvScalar(255, 0, 0)); */
		imshow("Image with line trail", imgOriginal);
		imshow("Blue threshold", bluehsv);
		cv::waitKey(1);
	}
	return 0;
}